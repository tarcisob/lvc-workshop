from time import mktime, strptime
from os import listdir
from os.path import isfile, join
import subprocess, sys

exifBin = "/usr/bin/exif"
tag = "--tag=DateTime"
machineReadableOption = "-m"

#onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]

def getDirFiles(dirPath):
	files = []
	for f in listdir(dirPath):
		filePath = join(dirPath,f)
		if isfile(filePath):
			files.append(filePath)

	return files

def getImgTimestamp(imgPath):
	proc = subprocess.Popen([exifBin, imgPath, tag, machineReadableOption], stdout=subprocess.PIPE, shell=False)
	(t, err) = proc.communicate()

	t = t.replace("\n","")
	timeStruct = strptime(t, "%Y:%m:%d %H:%M:%S")
	timestamp = mktime(timeStruct)

	return timestamp

if (len(sys.argv) != 2):
	#print "Usage: python simple-grouping-tool.py folderPath"
	print "Usage: python simple-grouping-tool.py imgPath"
	sys.exit()

#dirFiles = getDirFiles(sys.argv[1])

#for imgPath in dirFiles:
print getImgTimestamp(sys.argv[1])


