<html>
	<head>
		<link rel="stylesheet" type="text/css" href="lvc-ws.css">
		<title>LVC Workshop</title>
	</head> 
	<body>
<?php
	if ($_FILES["file"]["error"] > 0) {
	  echo "Error: " . $_FILES["file"]["error"] . "<br>";
	} else {
	  //echo "Stored in: " . $_FILES["file"]["tmp_name"];

		//Magic happens here!
		$execCmd = "/usr/bin/python /var/www/lvc-workshop/simple-grouping-tool.py";
		$uploadedImgTimestamp = exec($execCmd . " " . $_FILES["file"]["tmp_name"]);
	
		$myfile = fopen("/var/www/lvc-workshop/images-timestamps.txt", "r") or die("Unable to open file!");

		$localTimestamps = array();

		// Output one line until end-of-file
		while(!feof($myfile)) {
			$line = fgets($myfile);

			if (strlen($line) >0) {
				list($imgFile,$timestamp) = split(" ", $line);
				//echo $imgFile . " -> Timestamp: " . $timestamp . "\r\n";
				$localTimestamps[$imgFile] = abs($timestamp - $uploadedImgTimestamp);
			}
		}
		fclose($myfile);

		$imagesBaseFolderPath = "images/";

		asort($localTimestamps);
		foreach($localTimestamps as $img => $timeOffset) {
			//echo $img . " -> " . $timeOffset;
			$imgPath = "\"" . $imagesBaseFolderPath . $img . "\"";
			echo $imgPath;
			echo "<br>";		
			$imgSrc = "<img src=" . $imgPath . " width=320 height=240> ";
			echo $imgSrc;
			echo "<br>";
		}	

	}
?>
	<h2>Hope you're enjoying LVC Workshop!</h2>

	</body>
</html> 
